from src.vehicles import Car


def test_new_created_car_has_mileage_0():
    test_car = Car("New Car", 2010)
    assert test_car.mileage == 0


def test_car_mileage_100_after_drive_100():
    test_car = Car("New Car", 2010)
    test_car.drive(100)
    assert test_car.mileage == 100


def test_car_mileage_300_after_drive_100_and_200():
    test_car = Car("New Car", 2010)
    test_car.drive(100)
    test_car.drive(200)
    assert test_car.mileage == 300


def test_car_year_2020_mileage_119000_has_warranty():
    test_car = Car("New Car", 2020)
    test_car.drive(119000)
    assert test_car.has_warranty() is True


def test_car_year_2007_mileage_19000_has_warranty():
    test_car = Car("New Car", 2007)
    test_car.drive(19000)
    assert test_car.has_warranty() is False


def test_car_honda_civic_year_1998_mileage_230000_description():
    test_car = Car("Honda Civic", 1998)
    test_car.drive(230000)
    assert test_car.get_description() == f"This is a {test_car.model} made in {test_car.manufacture_year}. Currently it drove {test_car.mileage} kilometers."
