from datetime import date


class Car:
    def __init__(self, model, production_date):
        self.model = model
        self.manufacture_year = production_date
        self.mileage = 0

    def drive(self, distance):
        self.mileage += distance

    def has_warranty(self):
        return False if (date.today().year - self.manufacture_year > 7 or self.mileage > 120000) else True

    def get_description(self):
        return f"This is a {self.model} made in {self.manufacture_year}. Currently it drove {self.mileage} kilometers."


if __name__ == '__main__':
    first_car = Car("Peugeot 207", 2008)
    second_car = Car("Mitsubishi Outlander", 2018)
    first_car.drive(1500)
    second_car.drive(20000)
    print(f"Does {first_car.model} have warranty? - {first_car.has_warranty()}")
    print(f"Does {second_car.model} have warranty? - {second_car.has_warranty()}")
    print(first_car.get_description())
    print(second_car.get_description())
